<head>
    <link rel="stylesheet" href="style.css">
</head>

<form id="user-form" method="POST" action="">
    <h4>Name</h4>
    <input type="text" name="name">
    <h4>Email</h4>
    <input type="email" name="email">

    <div class="row">

        <!-- Birthday -->
        <div class="wrap">
            <h4>Birthday</h4>
            <select name="birthday">
                <?php for ($i = 1900; $i < 2020; $i++) { ?>
                    <option value="<?php print $i; ?>"><?= $i; ?></option>
                <?php } ?>
            </select>
        </div>

        <!-- Sex -->
        <div class="wrap">
            <h4>Sex</h4>
            <label>
                Male
                <input type="radio" name="sex" value="m">
            </label>
            <label>
                Female
                <input type="radio" name="sex" value="f">
            </label>
        </div>
    </div>

    <!-- Num of limbs -->
    <h4 class="limbs_header">Num of limbs</h4>
    <div class="limbs_wrap">
        <?php for ($i = 0; $i <= 4; $i++) { ?>
            <label><?= $i ?><input type="radio" name="limbs" value="<?= $i ?>"></label>
        <?php } ?>
    </div>

    <!-- skills -->
    <h4>Skills</h4>
    <select name="skills[]" multiple>
        <?php foreach ($skills_labels as $key => $value) { ?>
            <option value="<?= $key; ?>"><?= $value; ?></option>
        <?php } ?>
    </select>

    <!-- Biography -->
    <h4>Biography</h4>
    <textarea name="biography" cols="30" rows="10"></textarea>

    <!-- Contract accept -->
    <br>
    <label class="contract_accept">
        I agree with a contract
        <input type="checkbox" name="contract_accept">
    </label>

    <!-- Submit -->
    <input class="user-form__submit" type="submit" value="Submit" name="send">
</form>